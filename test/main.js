const fs = require('fs');
let noodle = require('../noodle');

noodle(
    fs.readFileSync(
        './test/main.noodle',
        {
            encoding: 'UTF-8'
        }
    ),

    {
        std: './std'
    }
);