module.exports = (t, i) => {
    let r = [];

    t.forEach(ct => {
        if(ct.type === 'identifier') {
            Object.keys(i).forEach(ci => {
               if(new RegExp(ci).test(ct.data)) {
                   ct.type = 'keyword';
                   ct.data = i[ci].type;
               }
            });
        }

        r.push(ct);
    });

    return r;
};