module.exports = l => {
    let r = [];

    l.forEach(cl => {
        if(cl.type === 'undefined') {
            cl.data.split(' ').forEach(cll => {
                if(/^[a-zA-Z]+$/.test(cll)) {
                    r.push({
                        type: 'identifier',
                        data: cll
                    })
                } else {
                    let cs = '';

                    cll.split('').forEach((cllc, clli) => {
                        cs += cllc;

                        if(/^[a-zA-Z]+$/.test(cllc)) {
                            if(!/^[a-zA-Z]+$/.test(cll.split('')[clli])) {
                                r.push({
                                    type: 'identifier',
                                    data: cs
                                });

                                cs = '';
                            }
                        } else {
                            if(cs.substr(0, cs.length - 1).length > 0 ? /^[a-zA-Z]+$/.test(cs.substr(0, cs.length - 1)) : false) {
                                r.push({
                                    type: 'identifier',
                                    data: cs.substr(0, cs.length - 1)
                                });
                            }

                            r.push({
                                type: 'undefined',
                                data: cllc
                            });

                            cs = '';
                        }
                    });
                }
            });

            return;
        }

        r.push(cl);
    });

    return r;
};