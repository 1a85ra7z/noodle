module.exports = (t, i) => {
    let r = [];

    t.forEach(ct => {
        if(ct.type === 'operator' || ct.type === 'parenthesis' || ct.type === 'colon') {
            Object.keys(i).forEach(ci => {
                if(new RegExp(`\\${ci}`).test(ct.data)) {
                    ct.data = i[ci].type;
                }
            });
        }

        r.push(ct);
    });

    return r;
};