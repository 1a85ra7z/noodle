module.exports = (t) => {
    let r = [];

    let rx = {
        operator: new RegExp('\\+|-|\\*|/|<|>|<=|>=|=|=='),
        parenthesis: new RegExp('[()\[\\]{}]'),
        colon: new RegExp(':')
    };

    t.forEach(ct => {
        if(ct.type === 'undefined') {
            Object.keys(rx).forEach(cv => {
                if(rx[cv].test(ct.data)) {
                    r.push({
                        type: cv,
                        data: ct.data
                    });
                }
            });

            return;
        }

        r.push(ct);
    });

    return r;
};