module.exports = t => {
    let r = [];

    t.forEach(ct => {
       if(ct.type === 'undefined') {
           if(/[0-9]/.test(ct.data)) {
               if(r.length > 0 ? r[r.length - 1].type === 'digit' : false) {
                   r[r.length - 1].data += ct.data;
                   return;
               }

               r.push({
                   type: 'digit',
                   data: ct.data
               });

               return;
           }
       }

       r.push(ct);
    });

    return r;
};