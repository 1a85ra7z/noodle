module.exports = (l) => {
    let ns = [];
    let cl = '';
    let yn = '';
    let is = false;

    l.split('').forEach(ct => {
        if(/['"`]/.test(ct)) {
            if(is && ct === yn) {
                ns.push({
                    type: 'string',
                    data: `${cl.replace(/['"`.*+?^${}()|[\]\\]/g, '\\$&')}`
                });
            } else if(!is) {
                if(ns.length > 0 ? ns[ns.length - 1].type === 'undefined' : false) {
                    ns[ns.length - 1].data += cl;
                } else {
                    ns.push({
                        type: 'undefined',
                        data: cl
                    });
                }

                yn = ct;
            } else {
                cl += ct;
                return;
            }

            is = !is;
        } else if(is) {
            cl += ct;
        } else {
            if(ns.length > 0 ? ns[ns.length - 1].type === 'undefined' : false) {
                ns[ns.length - 1].data += ct;
            } else {
                ns.push({
                    type: 'undefined',
                    data: ct
                });
            }
        }
    });

    return ns;
};