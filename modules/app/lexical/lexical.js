const tokenizer = require('./tokenizer');
const strings = require('./strings');
const digits = require('./digits');
const detection = require('./detection');
const keywords = require('./keywords');
const operators = require('./operators');

module.exports = (code, kw, ops) => {
    if(typeof code === 'string') {
        let f = [];
        let dl = [];

        code.split('\n').forEach(cl => {
            if(!dl.includes(cl.length - cl.trimLeft().length) && cl.trim().length > 0) {
                dl.push(cl.length - cl.trimLeft().length);
            }
        });

        dl.sort((a, b) => {
            return a - b;
        });

        code.split('\n').forEach(cl => {
            if(cl.trim().length > 0) {
                f.push({
                    tokens: operators(keywords(detection(digits(tokenizer(
                        strings(cl.trimLeft())
                    ))), kw), ops),
                    depth: dl.indexOf(cl.length - cl.trimLeft().length),
                    string: cl.trim()
                });
            }
        });

        return f;
    } else {
        throw new Error('Wrong type of parameters for lexical analyzer');
    }
};