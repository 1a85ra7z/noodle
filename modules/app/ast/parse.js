module.exports = (t) => {
    let p = {
        type: 'sequence',
        depth: -1,
        children: [],
    };

    let wd = p;

    t.forEach(t => {
        if(t.depth > wd.depth) {
            let o = {
                parent: wd,
                children: [],

                ...t
            };

            wd.children.push(o);
            wd = o;
        } else if(t.depth < wd.depth) {
            let go = wd.depth - t.depth;
            let cwd = wd;

            while(go + 1 > 0) {
                cwd = cwd.parent;
                go--;
            }

            let o = {
                parent: wd,
                children: [],

                ...t
            };

            cwd.children.push(o);
            wd = o;
        } else {
            let o = {
                parent: wd.parent,
                children: [],

                ...t
            };

            wd.parent.children.push(o);
            wd = o;
        }
    });

    return p;
};