const parse = require('./parse');
const powder = require('./powder');

module.exports = (t, gw) => {
    let pwdr = powder(gw);

    t.forEach((ct, i) => {
       t[i].pwdr = pwdr(ct);
    });

    return parse(t);
};