module.exports = j => {
    let cuo = [];

    let p = s => {
        let rx = [];
        let pr = [];
        let um = [];

        s.match(/([[{])[a-zA-Z|:$*]*([\]}])/g).forEach((w, i) => {
            if(w.includes('$')) {
                w = w.replace('$', '');
                pr.push(i);
            }

            if(w.includes('*')) {
                w = w.replace('*', '');
                um.push(i);
            }

            if(/\[[a-zA-Z|:*]*]/.test(w)) {
                rx.push(
                    new RegExp(w.substr(1, w.length - 2))
                );
            } else {
                switch(w.substr(1, w.length - 2)) {
                    case 'space':
                        rx.push(/\s*/);
                        break;
                    case 'any':
                        rx.push(/\S*/);
                        break;
                }
            }
        });

        return {
            d: rx,
            p: pr,
            u: um
        };
    };

    let w = (k, mo) => {
        let fnl = {
            found: false,
            tokens: k
        };

        if((typeof k === 'object' ? typeof k.tokens === 'object' : false) && typeof mo === 'object') {
            mo.forEach(co => {
                if(typeof co === 'object') {
                    let o = true;
                    let ps = {};

                    let i = 0;
                    let y = 0;
                    let cmu = false;

                    while(y < k.tokens.length) {
                        if(i < co.syntax.length) {
                            let ct = k.tokens[y];
                            let ts = ct.type;

                            if(co.syntax[i].source.includes(':')) {
                                ts = `${ct.type}:${ct.data}`;
                            }

                            if(co.syntax[i].test(ts)) {
                                if(cmu) {
                                    if(co.parameters.includes(i)) {
                                        if(typeof ps[i] !== 'object') {
                                            ps[i] = [];
                                        }

                                        ps[i].push(ct);

                                        if(y < k.tokens.length && i < mo.length) {
                                            if(w(k.tokens.slice(y + 1), mo.slice(i + 1)).found) {
                                                y++;
                                            } else {
                                                i++;
                                                cmu = false;
                                            }
                                        } else {
                                            y++;
                                        }
                                    }
                                } else if (!cmu) {
                                    if (co.parameters.includes(i)) {
                                        if(typeof ps[i] !== 'object') {
                                            ps[i] = [];
                                        }

                                        ps[i].push(ct);
                                    }

                                    if (co.many.includes(i)) {
                                        cmu = true;
                                    } else {
                                        i++;
                                    }

                                    y++;
                                }
                            } else {
                                if(cmu) {
                                    i++;
                                    cmu = false;
                                } else {
                                    o = false;
                                    break;
                                }
                            }
                        }
                    }

                    Object.keys(ps).forEach(cps => {
                        if(ps[cps].length > 0) {
                            let rws = w(ps[cps], mo);

                            if(rws.found) {
                                ps[cps] = rws;
                            }
                        }
                    });

                    if(o) {
                        fnl = {
                            ...fnl,
                            type: co.definition,
                            parameters: ps,
                            found: !fnl.found
                        }
                    }
                }
            });
        }

        return fnl;
    };

    if(typeof j === 'object') {
        Object.keys(j).forEach(c => {
            let sy = p(j[c]);

            cuo.push({
                syntax: sy.d,
                definition: c,
                parameters: sy.p,
                many: sy.u
            });
        });

        return k => w(k, cuo);
    }
};