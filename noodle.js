const fs = require('fs');
const util = require('util');

let lexical = require('./modules/app/lexical/lexical');
let ast = require('./modules/app/ast/ast');
let semantic = require('./modules/app/semantic');
let vm = require('./modules/app/vm');

module.exports = (code, options) => {
    let o = {
        keywords: './data/keywords.powder',
        operators: './data/operators.powder',
        grammar: './data/grammar.powder',
        std: './std'
    };

    if(typeof options === 'object') {
        if(typeof options.keywords === 'string' ? options.keywords.length > 0 : false) {
            o.keywords = options.keywords;
        }

        if(typeof options.grammar === 'string' ? options.grammar.length > 0 : false) {
            o.grammar = options.grammar;
        }

        if(typeof options.operators === 'string' ? options.operators.length > 0 : false) {
            o.operators = options.operators;
        }

        if(typeof options.std === 'string' ? options.std.length > 0 : false) {
            o.std = options.std;
        }
    }

    if(typeof code === 'string') {
        if(fs.existsSync(o.keywords)) {
            try {
                let kw = JSON.parse(fs.readFileSync(o.keywords, {encoding:'UTF-8'}));
                let ops = JSON.parse(fs.readFileSync(o.operators, {encoding:'UTF-8'}));
                let gw = JSON.parse(fs.readFileSync(o.grammar, {encoding:'UTF-8'}));

                let s = ast(
                    lexical(
                        code,
                        kw,
                        ops
                    ),

                    gw
                );

                console.log(util.inspect(s, false, null, true))
            } catch(e) {
                throw e;
            }
        }
    } else {
        throw new Error('First parameter needs to be type \'string\'');
    }
};